# Tom's Toolkit

A DirectX 11 game engine that is ever evolving to add new interesting features and try to be somewhat useful  

## Getting Started

Simply download/clone this repo to your local machine and everything should work... should  
Inside the engine, you can control the main game from the `main.cpp` file. (Creating GameObjects, loading models, textures, shaders, processing user input, etc.)  
You can also create extra classes that encapsulate advanced/complex behaviour and control them from the `main.cpp` file  

### Prerequisites

Make sure you have Windows 8.1 or later to get rolling  

## Built With

* [DirectX 11](https://msdn.microsoft.com/en-us/library/windows/desktop/ff476147%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396) - All those awesome graphics
* [DirectX SimpleMath](https://github.com/Microsoft/DirectXTK/wiki/SimpleMath) - Makes the math look less ugly
* [AssImp](http://www.assimp.org/) - Handles all the model loading goodness

## Authors

* **Tom Royle** - *Me!* - [Tom Royle](http://tom-royle.co.uk)  

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
