#include "Model.h"
#include "Texture.h"

Model::Model()
{
	isActive = true;
	parent = nullptr;

	texture = nullptr;
}

Model::~Model()
{
}

void Model::RenderBuffers(ID3D11DeviceContext* device)
{
	mesh->RenderBuffers(device);
}

bool Model::HasParent()
{
	return false;
}

void Model::SetTexture(Texture* tex)
{
	texture = tex;
}

Texture* Model::GetTexture()
{
	return texture;
}

ID3D11ShaderResourceView* Model::GetShaderResource()
{
	if (texture)
		return texture->GetTexture();
	return nullptr;
}