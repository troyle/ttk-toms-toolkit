#include "AssimpMesh.h"

AssimpMesh::AssimpMesh(std::string filename)
{
	loaded = true;
	numMaterials = 0;
	numFaces = 0;
	numVerts = 0;

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(filename,  aiProcess_Triangulate |
														aiProcess_JoinIdenticalVertices);
	
	if (!scene)
	{
		errorMsg = importer.GetErrorString();
		loaded = false;
	}
	else
	{
		for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
		{
			aiMesh* loadedMesh = scene->mMeshes[i];
			numFaces = loadedMesh->mNumFaces;
			numVerts = numFaces * 3;
			mesh = new MeshData[numVerts];
			int vertIndex = 0;
			for (int f = 0; f < numFaces; f++)
			{
				const aiFace& face = loadedMesh->mFaces[f];
				for (int v = 0; v < 3; ++v)
				{
					mesh[vertIndex].pos.x = loadedMesh->mVertices[face.mIndices[v]].x;
					mesh[vertIndex].pos.y = loadedMesh->mVertices[face.mIndices[v]].y;
					mesh[vertIndex].pos.z = loadedMesh->mVertices[face.mIndices[v]].z;

					mesh[vertIndex].uv.x = loadedMesh->mTextureCoords[0][face.mIndices[v]].x;
					mesh[vertIndex].uv.y = loadedMesh->mTextureCoords[0][face.mIndices[v]].y;
					if (loadedMesh->HasNormals())
					{
						mesh[vertIndex].normal.x = loadedMesh->mNormals[face.mIndices[v]].x;
						mesh[vertIndex].normal.y = loadedMesh->mNormals[face.mIndices[v]].y;
						mesh[vertIndex].normal.z = loadedMesh->mNormals[face.mIndices[v]].z;
					}

					vertIndex++;
				}
			}
		}
	}
}

AssimpMesh::~AssimpMesh()
{
}

bool AssimpMesh::isLoaded()
{
	return loaded;
}

std::string AssimpMesh::GetErrorMessage()
{
	return errorMsg;
}

bool AssimpMesh::InitBuffers(ID3D11Device* device, unsigned long* indices, int indexSize)
{
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	if (!indices) indices = new unsigned long[numVerts];

	if (indexSize == 0)
	{
		for (int i = 0; i < numVerts; ++i)
			indices[i] = i;
	}

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(MeshData) * numVerts;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = mesh;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	if (FAILED(device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer))) return false;

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	if (indexSize == 0) indexBufferDesc.ByteWidth = sizeof(unsigned long) * numVerts;
	else indexBufferDesc.ByteWidth = sizeof(unsigned long) * indexSize;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	if (FAILED(device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer)))	return false;

	delete[] mesh;
	delete[] indices;

	return true;
}

void AssimpMesh::RenderBuffers(ID3D11DeviceContext* device)
{
	unsigned int stride = sizeof(MeshData);
	unsigned int offset = 0;

	device->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	device->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void AssimpMesh::Release()
{
	vertexBuffer->Release();
	indexBuffer->Release();
}