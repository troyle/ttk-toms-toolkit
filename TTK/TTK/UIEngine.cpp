#include "UIEngine.h"

#include <string>
#include "Helper.h"

UIEngine::UIEngine()
{
	
}

bool UIEngine::Init(/*ID3D11Device* device*/IDXGISwapChain* swapChain)
{
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &factory);

	savedSwapChain = swapChain;

	IDXGISurface* backBuffer;
	swapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));

	float dpiX, dpiY;
	factory->GetDesktopDpi(&dpiX, &dpiY);

	D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, 
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);

	HRESULT result = factory->CreateDxgiSurfaceRenderTarget(backBuffer,	&props,	&renderTarget);

	renderTarget->CreateSolidColorBrush(D2D1::ColorF(0.0f, 1.0f, 0.0f), &whiteBrush);

	CreateIndependantResources();

	return true;
}

bool UIEngine::CreateIndependantResources()
{
	static const WCHAR msc_fontName[] = L"Verdana";
	static const FLOAT msc_fontSize = 16;
	HRESULT hr;
	
	// Create a DirectWrite factory.
	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(textFactory),
		reinterpret_cast<IUnknown **>(&textFactory));
	if (SUCCEEDED(hr))
	{
		// Create a DirectWrite text format object.
		hr = textFactory->CreateTextFormat(
			msc_fontName,
			NULL,
			DWRITE_FONT_WEIGHT_NORMAL,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			msc_fontSize,
			L"", //locale
			&textFormat
		);
	}
	if (SUCCEEDED(hr))
	{
		// Center the text horizontally and vertically.
		textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
	}

	return hr;
}

bool UIEngine::RenderUI(int fps)
{
	//static const WCHAR sc_helloWorld[] = L"2000 FPS";
	std::string temp = std::to_string(fps) + " FPS";

	DXGI_SWAP_CHAIN_DESC swapDesc;
	savedSwapChain->GetDesc(&swapDesc);
	D2D1_SIZE_F targetSize = renderTarget->GetSize();

	renderTarget->BeginDraw();

	whiteBrush->SetTransform(D2D1::Matrix3x2F::Scale(targetSize));

	renderTarget->DrawText(Helper::String2WCHAR(temp), temp.length(), textFormat,
		D2D1::RectF(targetSize.width - 100.0f, 5.0f, targetSize.width, 35.0f),	whiteBrush);
	
	renderTarget->EndDraw();

	return true;
}

void UIEngine::Release()
{
	
}