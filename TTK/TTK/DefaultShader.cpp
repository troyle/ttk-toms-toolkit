#include "DefaultShader.h"

#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

bool DefaultShader::Init(ID3D11Device* device, HWND hwnd, LPCWSTR filename)
{
	ID3DBlob* errorBlob = 0;
	ID3DBlob* vertexBuffer = 0;
	ID3DBlob* pixelBuffer = 0;

	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;

	if (FAILED(D3DCompileFromFile(filename, NULL, NULL, "Vertex", "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &vertexBuffer, &errorBlob)))
	{
		if (errorBlob)
			OutputShaderError(errorBlob, hwnd, NULL);
		else
			MessageBox(hwnd, "Missing Shader File", "Shader Error", MB_OK);

		return false;
	}

	if (FAILED(D3DCompileFromFile(filename, NULL, NULL, "Pixel", "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pixelBuffer, &errorBlob)))
	{
		if (errorBlob)
			OutputShaderError(errorBlob, hwnd, NULL);
		else
			MessageBox(hwnd, "Missing Shader File", "Shader Error", MB_OK);

		return false;
	}

	// Create the vertex shader from the buffer.
	if (FAILED(device->CreateVertexShader(vertexBuffer->GetBufferPointer(), vertexBuffer->GetBufferSize(), NULL, &vertexShader)))
		return false;

	// Create the pixel shader from the buffer.
	if (FAILED(device->CreatePixelShader(pixelBuffer->GetBufferPointer(), pixelBuffer->GetBufferSize(), NULL, &pixelShader)))
		return false;

	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	if (FAILED(device->CreateInputLayout(polygonLayout, numElements,
		vertexBuffer->GetBufferPointer(), vertexBuffer->GetBufferSize(), &layout)))
		return false;

	vertexBuffer->Release();
	pixelBuffer->Release();

	D3D11_BUFFER_DESC bufferDesc1;

	bufferDesc1.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc1.ByteWidth = sizeof(MatrixBuffer);
	bufferDesc1.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc1.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc1.MiscFlags = 0;
	bufferDesc1.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	if (FAILED(device->CreateBuffer(&bufferDesc1, NULL, &matrixBuffer))) return false;

	D3D11_BUFFER_DESC bufferDesc2;

	bufferDesc2.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc2.ByteWidth = sizeof(LightBuffer);
	bufferDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc2.MiscFlags = 0;
	bufferDesc2.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	if (FAILED(device->CreateBuffer(&bufferDesc2, NULL, &lightBuffer))) return false;

	D3D11_SAMPLER_DESC samplerDesc;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	if (FAILED(device->CreateSamplerState(&samplerDesc, &sampleState)))
		return false;

	return true;
}

#include <fstream>
void DefaultShader::OutputShaderError(ID3DBlob* errorBlob, HWND hwnd, WCHAR* filename)
{
	char* compileErrors;
	unsigned long long bufferSize, i;
	std::ofstream fout;

	compileErrors = (char*)(errorBlob->GetBufferPointer());

	bufferSize = errorBlob->GetBufferSize();

	fout.open("shader-error.txt");

	for (i = 0; i<bufferSize; i++)
		fout << compileErrors[i];

	fout.close();

	errorBlob->Release();

	MessageBox(hwnd, "Error compiling shader.  Check shader-error.txt for message.", "One of your shaders", MB_OK);

	return;
}

bool DefaultShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, Matrix world, Matrix view, Matrix proj, ID3D11ShaderResourceView* texture, List<Light*>* lights)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBuffer* dataPtr1;
	LightBuffer* dataPtr2;

	unsigned int bufferNumber = 0;

	world = XMMatrixTranspose(world);
	view = XMMatrixTranspose(view);
	proj = XMMatrixTranspose(proj);

	if (FAILED(deviceContext->Map(matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource))) return false;

	dataPtr1 = (MatrixBuffer*)mappedResource.pData;

	dataPtr1->world = world;
	dataPtr1->view = view;
	dataPtr1->proj = proj;

	deviceContext->Unmap(matrixBuffer, 0);
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &matrixBuffer);

	deviceContext->PSSetShaderResources(0, 1, &texture);

	if (FAILED(deviceContext->Map(lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
		return false;

	dataPtr2 = (LightBuffer*)mappedResource.pData;

	int i = 0;

	for (auto light : *lights)
	{
		dataPtr2->position[i] = Vector4(light->position.x, light->position.y, light->position.z, 1.0f);
		dataPtr2->direction[i] = Vector4(light->direction.x, light->direction.y, light->direction.z, light->range);
		dataPtr2->colour[i] = Vector4(light->colour.r, light->colour.g, light->colour.b, light->intensity);
		++i;
	}
	if (i < MAX_LIGHTS)
		dataPtr2->colour[i] = Vector4(0.0f, 0.0f, 0.0f, -10.0f);

	deviceContext->Unmap(lightBuffer, 0);

	bufferNumber = 0;
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &lightBuffer);

	return true;
}

bool DefaultShader::Render(ID3D11DeviceContext* deviceContext, int index, Matrix world, Matrix view, Matrix proj, ID3D11ShaderResourceView* texture, List<Light*>* lights)
{
	if (!SetShaderParameters(deviceContext, world, view, proj, texture, lights))
		return false;

	deviceContext->IASetInputLayout(layout);

	deviceContext->VSSetShader(vertexShader, NULL, 0);
	deviceContext->PSSetShader(pixelShader, NULL, 0);

	deviceContext->DrawIndexed(index, 0, 0);

	return true;
}

void DefaultShader::Release()
{
	if (matrixBuffer)
		matrixBuffer->Release();

	if (lightBuffer)
		lightBuffer->Release();

	if (layout)
		layout->Release();

	if (pixelShader)
		pixelShader->Release();

	if (vertexShader)
		vertexShader->Release();
}