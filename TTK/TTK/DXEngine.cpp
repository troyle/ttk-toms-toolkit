#include "DXEngine.h"

#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

#include <assert.h>

DXEngine* pThis = nullptr;
LRESULT CALLBACK DXEngine::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{	
	switch (message)
	{	
	case WM_NCCREATE:
		pThis = static_cast<DXEngine*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);

		SetLastError(0);
		if (!SetWindowLongPtr(hWnd, -21, reinterpret_cast<LONG_PTR>(pThis)))
		{
			if (GetLastError() != 0)
				return FALSE;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;

	case WM_CLOSE:
		pThis->terminate = true;
		return 1;

	case WM_KILLFOCUS:
		Input::ResetKeys();
		return 10;

	case WM_SETFOCUS:
		Input::ResetKeys();
		return 10;	

	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
		DirectX::Keyboard::ProcessMessage(message, wParam, lParam);
		break;

	case WM_INPUT:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MOUSEWHEEL:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_MOUSEHOVER:
		DirectX::Mouse::ProcessMessage(message, wParam, lParam);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

DXEngine::DXEngine(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	swapChain = 0;
	device = 0;
	renderTargetView = 0;
	depthBuffer = 0;
	depthState = 0;
	depthView = 0;
	rasterState = 0;

	HWND hWnd;
	WNDCLASSEX wc;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = "WindowClass";

	RegisterClassEx(&wc);

	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	int horizontal = desktop.right;
	int vertical = desktop.bottom;

	horizontal = horizontal / 2 - WINDOW_WIDTH / 2;
	vertical = vertical / 2 - WINDOW_HEIGHT / 2;

	RECT rect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW, false, WS_EX_OVERLAPPEDWINDOW);

	hWnd = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
		"WindowClass",
		"Tom's Toolkit",
		WS_OVERLAPPEDWINDOW,
		horizontal, vertical,
		rect.right - rect.left, rect.bottom - rect.top,
		NULL,
		NULL,
		hInstance,
		this);

	ShowWindow(hWnd, nCmdShow);
	savedHWND = hWnd;
}

bool DXEngine::Init()
{
	IDXGIFactory* factory;
	IDXGIAdapter* adapter;
	IDXGIOutput* adapterOutput;
	unsigned int numModes;
	DXGI_ADAPTER_DESC adapterDesc;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	D3D_FEATURE_LEVEL featureLevel;
	ID3D11Texture2D* backBufferPtr;
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	D3D11_RASTERIZER_DESC rasterDesc;
	float fieldOfView, screenAspect;

	if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory))) return false;
	if (FAILED(factory->EnumAdapters(0, &adapter))) return false;
	if (FAILED(adapter->EnumOutputs(0, &adapterOutput))) return false;
	if (FAILED(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL))) return false;
	
#ifdef VSYNC
	DXGI_MODE_DESC* displayModeList;
	unsigned int numerator, denominator;
	displayModeList = new DXGI_MODE_DESC[numModes];
	if (!displayModeList) return false;

	if (FAILED(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUMODES_INTERLACED, &numModes, displayModeList))) return false;

	for (i = 0; i<numModes; i++)
	{
		if (displayModeList[i].Width == (unsigned int)WINDOW_WIDTH)
		{
			if (displayModeList[i].Height == (unsigned int)WINDOW_HEIGHT)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}
	delete[] displayModeList;
#endif

	if (FAILED(adapter->GetDesc(&adapterDesc))) return false;
	vRamSize = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	adapterOutput->Release();
	adapter->Release();
	factory->Release();

	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = WINDOW_WIDTH;
	swapChainDesc.BufferDesc.Height = WINDOW_HEIGHT;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

#ifdef VSYNC
	if (vsync_enabled)
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
#endif
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = savedHWND;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Windowed = true;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;

	featureLevel = D3D_FEATURE_LEVEL_11_1;

	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 
		D3D11_CREATE_DEVICE_BGRA_SUPPORT, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, 
		&device, NULL, &deviceContext))) return false;
	if (FAILED(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr))) return false;
	if (FAILED(device->CreateRenderTargetView(backBufferPtr, NULL, &renderTargetView))) return false;
	
	backBufferPtr->Release();

	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = WINDOW_WIDTH;
	depthBufferDesc.Height = WINDOW_HEIGHT;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	if (FAILED(device->CreateTexture2D(&depthBufferDesc, NULL, &depthBuffer))) return false;

	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	if (FAILED(device->CreateDepthStencilState(&depthStencilDesc, &depthState))) return false;
	deviceContext->OMSetDepthStencilState(depthState, 1);

	depthStencilDesc.DepthEnable = false;
	if (FAILED(device->CreateDepthStencilState(&depthStencilDesc, &depthDisabledState))) return false;

	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	if (FAILED(device->CreateDepthStencilView(depthBuffer, &depthStencilViewDesc, &depthView))) return false;
	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthView);

	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	//rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	if (FAILED(device->CreateRasterizerState(&rasterDesc, &rasterState))) return false;

	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	if (FAILED(device->CreateRasterizerState(&rasterDesc, &rasterWireframeState))) return false;

	deviceContext->RSSetState(rasterState);

	viewport.Width = WINDOW_WIDTH;
	viewport.Height = WINDOW_HEIGHT;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;

	deviceContext->RSSetViewports(1, &viewport);

	fieldOfView = (float)DirectX::XM_PI / 4.0f;
	screenAspect = (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT;

	projMatrix = DirectX::XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, NEAR_CLIP, FAR_CLIP);
	orthoMatrix = DirectX::XMMatrixOrthographicLH(WINDOW_WIDTH, WINDOW_HEIGHT, NEAR_CLIP, FAR_CLIP);
	worldMatrix = Matrix::Identity;

	defaultShader = new DefaultShader();
	defaultShader->Init(device, savedHWND, L"DefaultShader.hlsl");

	uiDisplay = new UIEngine();
	uiDisplay->Init(swapChain);

	Time::Init();
	Time::Update();
	
	Input::Init(savedHWND);
	Input::Update();
	
	return true;
}

void DXEngine::ZBufferOn()
{
	deviceContext->OMSetDepthStencilState(depthState, 1);
}

void DXEngine::ZBufferOff()
{
	deviceContext->OMSetDepthStencilState(depthDisabledState, 1);
}

void DXEngine::SolidView()
{
	deviceContext->RSSetState(rasterState);
}

void DXEngine::WireframeView()
{
	deviceContext->RSSetState(rasterWireframeState);
}

void DXEngine::Release()
{
	if (swapChain)			{ swapChain->SetFullscreenState(false, NULL); swapChain->Release(); }
	if (renderTargetView)	renderTargetView->Release();
	if (depthBuffer)		depthBuffer->Release();
	if (depthState)			depthState->Release();
	if (depthDisabledState)	depthDisabledState->Release();
	if (depthView)			depthView->Release();
	if (rasterState)		rasterState->Release();
	if (rasterWireframeState)rasterWireframeState->Release();
	if (device)				device->Release();
	if (deviceContext)		deviceContext->Release();

	if (defaultShader)		{ defaultShader->Release(); delete defaultShader; }

	if (modelList.size() > 0) modelList.clear();
	if (lightList.size() > 0) lightList.clear();

	if (uiDisplay)			{ uiDisplay->Release(); delete uiDisplay; }

	Helper::Release();
	AssetManager::Release();
}

void DXEngine::BeginScene()
{
	float colour[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	deviceContext->ClearRenderTargetView(renderTargetView, colour);
	deviceContext->ClearDepthStencilView(depthView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	Time::Update();
}

void DXEngine::EndScene()
{
	swapChain->Present(0, 0);
}

Camera* DXEngine::CreateCamera(Vector3 position)
{
	mainCamera = new Camera();
	mainCamera->SetBaseViewMatrix();
	mainCamera->transform.position = position;

	return mainCamera;
}

void DXEngine::SetCamera(Camera* cam)
{
	mainCamera = cam;
}

Model* DXEngine::CreateModel(std::string filename)
{
	AssimpMesh* newMesh = AssetManager::CreateMesh(device, filename);

	if (newMesh)
	{
		Model* newModel = new Model();
		newModel->mesh = newMesh;
		modelList.push_back(newModel);
		return newModel;
	}
	else
		return nullptr;
}

Model* DXEngine::CreateModel(Primitive primitiveType, int resolution)
{
	std::string filename;
	switch (primitiveType)
	{
		case Primitive::Cube:
			if (resolution == 0)
				resolution = 2;
			filename = "Cube__";
			break;
		case Primitive::Plane:
			if (resolution == 0)
				resolution = 1;
			filename = "Plane__";
			break;
	}

	filename += std::to_string(resolution);

	AssimpMesh* newMesh = AssetManager::CreateMesh(device, filename, primitiveType, resolution);

	if (newMesh)
	{
		Model* newModel = new Model();
		newModel->mesh = newMesh;
		modelList.push_back(newModel);
		return newModel;
	}
	else
		return nullptr;
}

Texture* DXEngine::CreateTexture(std::string filename)
{
	return AssetManager::CreateTexture(device, filename);
}

DirectionalLight* DXEngine::CreateDirectionalLight()
{
	if (lightList.size() < MAX_LIGHTS)
	{
		DirectionalLight* newLight = new DirectionalLight();

		lightList.push_back(newLight);
		return newLight;
	}
	else
		return nullptr;
}

PointLight* DXEngine::CreatePointLight(Vector3 position)
{
	if (lightList.size() < MAX_LIGHTS)
	{
		PointLight* newLight = new PointLight();
		newLight->position = position;

		lightList.push_back(newLight);
		return newLight;
	}
	else
		return nullptr;
}

void DXEngine::SetBackBufferRenderTarget()
{
	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthView);
}

void DXEngine::ResetViewport()
{
	deviceContext->RSSetViewports(1, &viewport);
}

float timer = 1.0f;
int fps = 0;
void DXEngine::Render()
{
	Input::Update();

	mainCamera->Update();
	mainCamera->GetMatrix(&viewMatrix);

	BeginScene();
	
	for(auto model : modelList)
	{
		if (model->isActive)
		{
			Matrix modelMatrix;
			model->RenderBuffers(deviceContext);
			model->transform.GetMatrix(&modelMatrix);

			defaultShader->Render(deviceContext, model->mesh->numVerts, modelMatrix, viewMatrix, projMatrix,
			model->GetShaderResource(), &lightList);
		}
	}

	timer += Time::deltaTime;
	if (timer >= 1.0f)
	{
		timer = 0.0f;
		fps = 1 / Time::deltaTime;
	}

	uiDisplay->RenderUI(fps);

	EndScene();
}

bool DXEngine::IsRunning()
{
	if (!terminate)
	{
		MSG msg;
		ZeroMemory(&msg, sizeof(MSG));
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		return true;
	}
	else
	{
		Release();
		return false;
	}
}

void DXEngine::Close()
{
	terminate = true;
}