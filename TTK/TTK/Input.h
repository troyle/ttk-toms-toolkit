#pragma once

#include <Windows.h>

#include <DirectXTK/Mouse.h>
#include <DirectXTK/Keyboard.h>
//#include "Keys.h"

#include "Maths.h"

class DXEngine;

enum class CursorMode
{
	Free,
	Locked
};

typedef DirectX::Keyboard::Keys Key;

class Input
{
	friend class DXEngine;

private:
	static std::unique_ptr<DirectX::Keyboard> keyboard;
	static DirectX::Keyboard::State keyboardState;
	static DirectX::Keyboard::KeyboardStateTracker keyboardTracker;
	static std::unique_ptr<DirectX::Mouse> mouse;

	static void Init(HWND hwnd);
	static void Update();
	static void ResetKeys();
	
public:
	static bool GetKey(Key input);		// Key Held
	static bool GetKeyDown(Key input);	// Key Hit
	static bool GetKeyUp(Key input);	// Key Released
	static Vector2 GetMouse();			// Get Mouse Position

	// CursorMode::Free		GetMouse returns pixel location on screen
	// CursorMode::Locked	GetMouse returns relative mouse movement
	static void MouseMode(CursorMode mode);
};
