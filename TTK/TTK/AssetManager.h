#pragma once

#include <string>
#include <map>

#include "Model.h"
#include "Texture.h"

typedef unsigned int uid;

enum class Primitive
{
	Cube,
	Plane,
};

class AssetManager
{
private:
	static std::map <std::string, AssimpMesh*> meshMap;
	static std::map <std::string, Texture*> textureMap;

public:
	AssetManager();
	~AssetManager();

	static AssimpMesh* CreateMesh(ID3D11Device* device, std::string filename, Primitive primitiveType = Primitive::Cube, int resolution = -1);
	static Texture* CreateTexture(ID3D11Device* device, std::string filename);
	//Audio?

	static void Release();
};

