#include "DXEngine.h"

//Leak Detector
//#include <vld.h>

int WINAPI WinMain(HINSTANCE hInstance,	HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	DXEngine* TTK = new DXEngine(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	if (!TTK->Init()) return -1;

	//////////////////////////////
	// Game Setup Happens Here
	Camera* camera = TTK->CreateCamera(Vector3(-1.0f, 1.0f, -3.0f));
	camera->transform.eulerAngles = Vector3(20.0f, 20.0f, 0.0f);

	Input::MouseMode(CursorMode::Locked);

	DirectionalLight* sun = TTK->CreateDirectionalLight();
	sun->direction = Vector3(0.0f, -1.0f, 1.0f);

	Texture* texture = TTK->CreateTexture("resources\\DefaultTexture.png");

	Model* cube = TTK->CreateModel(Primitive::Cube);
	cube->transform.eulerAngles = Vector3(0.0f, -10.0f, 0.0f);
	cube->SetTexture(texture);

	//TTK->WireframeView();
	
	while (TTK->IsRunning())
	{
		//////////////////////////////
		// Game Stuff Happens Here

		//Press Escape to close
		if (Input::GetKeyDown(Key::Escape)) TTK->Close();

		TTK->Render();
	}
	
	delete camera;
	delete TTK;

	return 0;
}