#pragma once

#include "Transform.h"
#include "Colour.h"

#define MAX_LIGHTS 1000

class DXEngine;

enum class LightType { Directional, Point, Spot };

class Light : public Transform
{
protected:	

public:
	bool isActive;

	Colour colour;
	float intensity;
	float range;
	Vector3 direction;

	Light() {}
	virtual ~Light();
};

inline Light::~Light() {}