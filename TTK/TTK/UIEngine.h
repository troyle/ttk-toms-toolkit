#pragma once

#include <windows.h>
#include <d3d11.h>
#include <d2d1.h>
#include <dwrite.h>
#include <d2d1helper.h>

#pragma comment(lib, "d2d1")
#pragma comment(lib, "Dwrite")

class UIEngine
{
private:
	IDXGISwapChain* savedSwapChain;
	ID3D11Texture2D* renderTexture;

	ID2D1Factory* factory;
	ID2D1RenderTarget* renderTarget;

	ID2D1SolidColorBrush* whiteBrush;

	IDWriteFactory* textFactory;
	IDWriteTextFormat* textFormat;

public:
	UIEngine();
	~UIEngine() {}

	bool Init(/*ID3D11Device* device*/IDXGISwapChain* swapChain);
	void Release();

	bool CreateIndependantResources();

	bool RenderUI(int fps);
};

