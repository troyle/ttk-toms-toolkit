#pragma once

#include <Windows.h>
#include <d3d11.h>
#include "Transform.h"
#include "AssimpMesh.h"

class DXEngine;
class Texture;
	
class Model
{
private:
	friend DXEngine;
	
	AssimpMesh* mesh;
	Texture* texture;

	Model();

	void RenderBuffers(ID3D11DeviceContext* device);
	ID3D11ShaderResourceView* GetShaderResource();

public:

	bool isActive;
	Transform* parent;

	Transform transform;

	~Model();

	bool HasParent();

	void SetTexture(Texture* tex);
	Texture* GetTexture();
};

