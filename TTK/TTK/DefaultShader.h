#pragma once

#include "Maths.h"
#include "Light.h"

#include "Containers.h"

class DefaultShader
{
private:
	struct MatrixBuffer
	{
		Matrix world;
		Matrix view;
		Matrix proj;
	};

	struct LightBuffer
	{
		Vector4 position[MAX_LIGHTS];
		Vector4 direction[MAX_LIGHTS];
		Vector4 colour[MAX_LIGHTS];
	};

	ID3D11VertexShader* vertexShader = 0;
	ID3D11PixelShader* pixelShader = 0;
	ID3D11InputLayout* layout;
	ID3D11SamplerState* sampleState;
	ID3D11Buffer* matrixBuffer;
	ID3D11Buffer* lightBuffer;

	void OutputShaderError(ID3DBlob* errorMessage, HWND hwnd, WCHAR* filename);

	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, 
		Matrix world, Matrix view, Matrix proj, 
		ID3D11ShaderResourceView* texture, List<Light*>* lights);

public:

	DefaultShader() {}
	~DefaultShader() {}

	bool Init(ID3D11Device* device, HWND hwnd, LPCWSTR filename);
	void Release();

	bool Render(ID3D11DeviceContext* deviceContext, int index, 
		Matrix world, Matrix view, Matrix proj, 
		ID3D11ShaderResourceView* texture, List<Light*>* lights);
};

