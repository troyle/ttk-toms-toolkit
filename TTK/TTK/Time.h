#pragma once

#include <Windows.h>

class DXEngine;

//Time class for frame speed
class Time
{
	friend DXEngine;

private:
	static INT64 frequency, startTime;
	static float ticksPerMs, _deltaTime;
	
	Time()				{}
	~Time()				{}
	Time(const Time&)	{}

	static bool Init();
	static void Update();
public:
	static float deltaTime;
	static float timeScale;
};

