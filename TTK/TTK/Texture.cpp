#include <DirectXTK/WICTextureLoader.h>

#include "Texture.h"

bool Texture::Init(ID3D11Device* device, std::string filename)
{
	if (FAILED(DirectX::CreateWICTextureFromFile(device, Helper::String2WCHAR(filename), nullptr, &texture)))
		return false;

	return true;
}

void Texture::Release() 
{
	if (texture)
		texture->Release();
}

ID3D11ShaderResourceView* Texture::GetTexture()
{
	return texture;
}

