#pragma once

#include <vector>

template<typename T>
using List = std::vector<T>;

//template<typename T, typename U>
//using Dictionary = std::map<T, U>;