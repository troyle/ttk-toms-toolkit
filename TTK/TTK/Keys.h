#pragma once

enum KeyState
{
	off,
	hit,
	held,
	up
};

enum class Key
{ 
	Key_LBUTTON			= 0x01,	//	Left mouse button
	Key_RBUTTON			= 0x02,	//	Right mouse button
	Key_MBUTTON			= 0x04,	//	Middle mouse button(three - button mouse)
	Key_XBUTTON1		= 0x05,	//	X1 mouse button
	Key_XBUTTON2		= 0x06,	//	X2 mouse button
	Key_BACK			= 0x08,	//	BACKSPACE key
	Key_TAB				= 0x09,	//	TAB key
	Key_RETURN			= 0x0D,	//	ENTER key
	Key_SHIFT			= 0x10,	//	SHIFT key
	Key_CONTROL			= 0x11,	//	CTRL key
	Key_MENU			= 0x12,	//	ALT key
	Key_CAPITAL			= 0x14,	//	CAPS LOCK key
	Key_ESCAPE			= 0x1B,	//	ESC key
	Key_SPACE			= 0x20,	//	SPACEBAR
	Key_PRIOR			= 0x21,	//	PAGE UP key
	Key_NEXT			= 0x22,	//	PAGE DOWN key
	Key_END				= 0x23,	//	END key
	Key_HOME			= 0x24,	//	HOME key
	Key_LEFT			= 0x25,	//	LEFT ARROW key
	Key_UP				= 0x26,	//	UP ARROW key
	Key_RIGHT			= 0x27,	//	RIGHT ARROW key
	Key_DOWN			= 0x28,	//	DOWN ARROW key
	Key_SELECT			= 0x29,	//	SELECT key
	Key_SNAPSHOT		= 0x2C,	//	PRINT SCREEN key
	Key_INSERT			= 0x2D,	//	INS key
	Key_DELETE			= 0x2E,	//	DEL key
	Key_0				= 0x30,	//	0 key
	Key_1				= 0x31,	//	1 key
	Key_2				= 0x32,	//	2 key
	Key_3				= 0x33,	//	3 key
	Key_4				= 0x34,	//	4 key
	Key_5				= 0x35,	//	5 key
	Key_6				= 0x36,	//	6 key
	Key_7				= 0x37,	//	7 key
	Key_8				= 0x38,	//	8 key
	Key_9				= 0x39,	//	9 key
	Key_A				= 0x41,	//	A key
	Key_B				= 0x42,	//	B key
	Key_C				= 0x43,	//	C key
	Key_D				= 0x44,	//	D key
	Key_E				= 0x45,	//	E key
	Key_F				= 0x46,	//	F key
	Key_G				= 0x47,	//	G key
	Key_H				= 0x48,	//	H key
	Key_I				= 0x49,	//	I key
	Key_J				= 0x4A,	//	J key
	Key_K				= 0x4B,	//	K key
	Key_L				= 0x4C,	//	L key
	Key_M				= 0x4D,	//	M key
	Key_N				= 0x4E,	//	N key
	Key_O				= 0x4F,	//	O key
	Key_P				= 0x50,	//	P key
	Key_Q				= 0x51,	//	Q key
	Key_R				= 0x52,	//	R key
	Key_S				= 0x53,	//	S key
	Key_T				= 0x54,	//	T key
	Key_U				= 0x55,	//	U key
	Key_V				= 0x56,	//	V key
	Key_W				= 0x57,	//	W key
	Key_X				= 0x58,	//	X key
	Key_Y				= 0x59,	//	Y key
	Key_Z				= 0x5A,	//	Z key
	Key_LWIN			= 0x5B,	//	Left Windows key(Natural keyboard)
	Key_RWIN			= 0x5C,	//	Right Windows key(Natural keyboard)
	Key_NUMPAD0			= 0x60,	//	Numeric keypad 0 key
	Key_NUMPAD1			= 0x61,	//	Numeric keypad 1 key
	Key_NUMPAD2			= 0x62,	//	Numeric keypad 2 key
	Key_NUMPAD3			= 0x63,	//	Numeric keypad 3 key
	Key_NUMPAD4			= 0x64,	//	Numeric keypad 4 key
	Key_NUMPAD5			= 0x65,	//	Numeric keypad 5 key
	Key_NUMPAD6			= 0x66,	//	Numeric keypad 6 key
	Key_NUMPAD7			= 0x67,	//	Numeric keypad 7 key
	Key_NUMPAD8			= 0x68,	//	Numeric keypad 8 key
	Key_NUMPAD9			= 0x69,	//	Numeric keypad 9 key
	Key_MULTIPLY		= 0x6A,	//	Multiply key
	Key_ADD				= 0x6B,	//	Add key
	Key_SEPARATOR		= 0x6C,	//	Separator key
	Key_SUBTRACT		= 0x6D,	//	Subtract key
	Key_DECIMAL			= 0x6E,	//	Decimal key
	Key_DIVIDE			= 0x6F,	//	Divide key
	Key_F1				= 0x70,	//	F1 key
	Key_F2				= 0x71,	//	F2 key
	Key_F3				= 0x72,	//	F3 key
	Key_F4				= 0x73,	//	F4 key
	Key_F5				= 0x74,	//	F5 key
	Key_F6				= 0x75,	//	F6 key
	Key_F7				= 0x76,	//	F7 key
	Key_F8				= 0x77,	//	F8 key
	Key_F9				= 0x78,	//	F9 key
	Key_F10				= 0x79,	//	F10 key
	Key_F11				= 0x7A,	//	F11 key
	Key_F12				= 0x7B,	//	F12 key
	Key_F13				= 0x7C,	//	F13 key
	Key_F14				= 0x7D,	//	F14 key
	Key_F15				= 0x7E,	//	F15 key
	Key_F16				= 0x7F,	//	F16 key
	Key_F17				= 0x80,	//	F17 key
	Key_F18				= 0x81,	//	F18 key
	Key_F19				= 0x82,	//	F19 key
	Key_F20				= 0x83,	//	F20 key
	Key_F21				= 0x84,	//	F21 key
	Key_F22				= 0x85,	//	F22 key
	Key_F23				= 0x86,	//	F23 key
	Key_F24				= 0x87,	//	F24 key
	Key_NUMLOCK			= 0x90,	//	NUM LOCK key
	Key_SCROLL			= 0x91,	//	SCROLL LOCK key
	Key_LSHIFT			= 0xA0,	//	Left SHIFT key
	Key_RSHIFT			= 0xA1,	//	Right SHIFT key
	Key_LCONTROL		= 0xA2,	//	Left CONTROL key
	Key_RCONTROL		= 0xA3,	//	Right CONTROL key
	Key_LMENU			= 0xA4,	//	Left MENU key
	Key_RMENU			= 0xA5,	//	Right MENU key
	Key_VOLUME_MUTE		= 0xAD,	//	Volume Mute key
	Key_VOLUME_DOWN		= 0xAE,	//	Volume Down key
	Key_VOLUME_UP		= 0xAF,	//	Volume Up key
	Key_OEM_1			= 0xBA,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the ';:' key
	Key_OEM_PLUS		= 0xBB,	//	For any country / region the '+' key
	Key_OEM_COMMA		= 0xBC,	//	For any country / region the '' key
	Key_OEM_MINUS		= 0xBD,	//	For any country / region the '-' key
	Key_OEM_PERIOD		= 0xBE,	//	For any country / region the '.' key
	Key_OEM_2			= 0xBF,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the '/?' key
	Key_OEM_3			= 0xC0,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the '`~' key
	Key_OEM_4			= 0xDB,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the '[{' key
	Key_OEM_5			= 0xDC,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the '\|' key
	Key_OEM_6			= 0xDD,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the ']}' key
	Key_OEM_7			= 0xDE,	//	Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard the 'single-quote/double-quote' key
	Key_OEM_8			= 0xDF,	//	Used for miscellaneous characters; it can vary by keyboard.
	Key_OEM_102			= 0xE2,	//	Either the angle bracket key or the backslash key on the RT 102 - key keyboard
	Close_Window,
	NumKeys
};