#pragma once

#include <d3d11.h>
#include <DirectXTK/SimpleMath.h>
#include <cstdlib>

#pragma warning( push )
#pragma warning( disable : 4244)

using namespace DirectX::SimpleMath;

namespace DirectX
{
	namespace SimpleMath
	{
#define XM_HALFPI DirectX::XM_PI/2

		static inline float Map(float val, float min1, float max1, float min2, float max2)
		{
			return (val - min1) / (max1 - min1) * (max2 - min2) + min2;
		}

		static const Vector3 GlobalUp = Vector3(0.0f, 1.0f, 0.0f);
		static const Vector3 GlobalForward = Vector3(0.0f, 0.0f, 1.0f);
		static const Vector3 GlobalRight = Vector3(1.0f, 0.0f, 0.0f);
	}
}

class Maths
{
private:
	/////////////////////////////////////////////////////////
	// Perlin Noise
	static float permutation[];
	static float fade(float t) { return t * t * t * (t * (t * 6 - 15) + 10); }
	static float lerp(float t, float a, float b) { return a + t * (b - a); }
	static float grad(int hash, float x, float y, float z) {
		int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
		float u = h < 8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
			v = h < 4 ? y : h == 12 || h == 14 ? x : z;
		return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
	}
	static float perlinNoise(float x, float y, float z) {
		int X = (int)floor(x) & 255;                  // FIND UNIT CUBE THAT
		int Y = (int)floor(y) & 255;                  // CONTAINS POINT.
		int Z = (int)floor(z) & 255;
		x -= floor(x);                                // FIND RELATIVE X,Y,Z
		y -= floor(y);                                // OF POINT IN CUBE.
		z -= floor(z);
		float u = fade(x),                                // COMPUTE FADE CURVES
			v = fade(y),                                // FOR EACH OF X,Y,Z.
			w = fade(z);
		int A = permutation[X] + Y, AA = permutation[A] + Z, AB = permutation[A + 1] + Z,      // HASH COORDINATES OF
			B = permutation[X + 1] + Y, BA = permutation[B] + Z, BB = permutation[B + 1] + Z;      // THE 8 CUBE CORNERS,

		return	lerp(w, lerp(v, lerp(u, grad(permutation[AA], x, y, z),  // AND ADD
			grad(permutation[BA], x - 1, y, z)), // BLENDED
			lerp(u, grad(permutation[AB], x, y - 1, z),  // RESULTS
				grad(permutation[BB], x - 1, y - 1, z))),// FROM  8
			lerp(v, lerp(u, grad(permutation[AA + 1], x, y, z - 1),  // CORNERS
				grad(permutation[BA + 1], x - 1, y, z - 1)), // OF CUBE
				lerp(u, grad(permutation[AB + 1], x, y - 1, z - 1),
					grad(permutation[BB + 1], x - 1, y - 1, z - 1))));
	}
	float octaveNoise(float x, float y, float z, int octaves, float persistence)
	{
		float total = 0;
		float frequency = 1;
		float amplitude = 1;
		float maxValue = 0;
		for (int i = 0; i < octaves; i++) {
			total += perlinNoise(x * frequency, y * frequency, z * frequency) * amplitude;

			maxValue += amplitude;

			amplitude *= persistence;
			frequency += frequency;
		}

		return total / maxValue;
	}
public:
	const static float Infinity;

	static float Random()
	{
		return static_cast<float> (rand()) / static_cast<float> (RAND_MAX);
	}

	static float Random(float min, float max)
	{
		return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));
	}

	static int Random(int min, int max)
	{
		return rand() % max + min;
	}

	static float Map(float val, float min1, float max1, float min2, float max2)
	{
		return (val - min1) / (max1 - min1) * (max2 - min2) + min2;
	}

	static float PerlinNoise(float x)
	{
		return perlinNoise(x, 0.0f, 0.0f);
	}

	static float PerlinNoise(float x, float y)
	{
		return perlinNoise(x, y, 0.0f);
	}

	static float PerlinNoise(float x, float y, float z)
	{
		return perlinNoise(x, y, z);
	}
};

#pragma warning( pop ) 