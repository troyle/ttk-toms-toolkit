#define MAX_LIGHTS 1000

cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projMatrix;
};

cbuffer LightBuffer
{
	float4 lightPosition[MAX_LIGHTS];
	float4 lightDirection[MAX_LIGHTS];
	float4 lightColour[MAX_LIGHTS];
};

struct vIn
{
	float4 position : POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
};

struct pIn
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;

	float4 worldPos : TEXCOORD1;
};

Texture2D colourTexture		: register(t0);
SamplerState SamplePoint	: register(s0);

pIn Vertex(vIn input)
{
	pIn output;

	input.position.w = 1.0f;

	output.position = mul(input.position, worldMatrix);
	output.worldPos = output.position;
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projMatrix);

	output.uv = input.uv;

	output.normal = normalize(mul(input.normal, (float3x3)worldMatrix));
	
	return output;
}

float4 Pixel(pIn input) : SV_TARGET
{
	float4 ambient = { 0.1f, 0.1f, 0.1f, 1.0f };

	float4 colours;
	float4 textureSample;
	float exposure;

	float4 combinedLights = { 0.0f, 0.0f, 0.0f, 1.0f };

	textureSample = colourTexture.Sample(SamplePoint, input.uv);

	colours = textureSample * ambient;

	for (int i = 0; i < MAX_LIGHTS; ++i)
	{
		if (lightColour[i].w < -1.0f)
			break;

		//Point Light
		if (lightDirection[i].w > -1)
		{
			float3 lightPos = lightPosition[i].xyz - input.worldPos.xyz;

			float attenuation = 1.0f - saturate(length(lightPos) / lightDirection[i].w);
			exposure = saturate(dot(input.normal, normalize(lightPos)) * pow(attenuation, 2));
		}
		else //Directional Light
		{
			exposure = saturate(dot(input.normal, -normalize(lightDirection[i].xyz)));
		}

		if (exposure > 0.0f)
			combinedLights.xyz += (lightColour[i].xyz * lightColour[i].w) * exposure;
	}
	
	colours += textureSample * saturate(combinedLights);

	return saturate(colours);
}