#pragma once
#include "Light.h"

class PointLight : public Light
{
	friend DXEngine;

private:
	Vector3 direction;

public:
	PointLight();
	~PointLight() {}
};

