#include "PointLight.h"

PointLight::PointLight()
{
	isActive = true;

	range = 10.0f;

	colour = Colour::White;
	intensity = 1.0f;
	direction = Vector3(-1.0f, -1.0f, 0.0f);
}

