#include "Colour.h"

Colour Colour::Red		= Colour(1.0f, 0.0f, 0.0f);
Colour Colour::Green	= Colour(0.0f, 1.0f, 0.0f);
Colour Colour::Blue		= Colour(0.0f, 0.0f, 1.0f);
Colour Colour::Black	= Colour(0.0f, 0.0f, 0.0f);
Colour Colour::White	= Colour(1.0f, 1.0f, 1.0f);

Colour::Colour()
{
	r = 0.0f;
	g = 0.0f;
	b = 0.0f;
	a = 1.0f;
}

Colour::Colour(float Shade)
{
	r = Shade;
	g = Shade;
	b = Shade;
	a = 1.0f;
}

Colour::Colour(float R, float G, float B)
{
	r = R;
	g = G;
	b = B;
	a = 1.0f;
}

Colour::Colour(float R, float G, float B, float A)
{
	r = R;
	g = G;
	b = B;
	a = A;
}

Vector3 Colour::AsVector3()
{
	return Vector3(r, g, b);
}

Vector4 Colour::AsVector4()
{
	return Vector4(r, g, b, a);
}