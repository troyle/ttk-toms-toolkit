#pragma once

#include "Light.h"

class DirectionalLight : public Light
{
	friend DXEngine;

private:
	float range;

	DirectionalLight();

public:
	~DirectionalLight() {}
};

