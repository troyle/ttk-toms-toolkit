#include "Input.h"

//KeyState Input::keyStates[(int)Key::NumKeys];
std::unique_ptr<DirectX::Keyboard> Input::keyboard;
DirectX::Keyboard::State Input::keyboardState;
DirectX::Keyboard::KeyboardStateTracker Input::keyboardTracker;
std::unique_ptr<DirectX::Mouse> Input::mouse;

void Input::Init(HWND hwnd)
{
	keyboard = std::make_unique<DirectX::Keyboard>();
	mouse = std::make_unique<DirectX::Mouse>();
	mouse->SetWindow(hwnd);
}

void Input::Update()
{
	keyboardState = DirectX::Keyboard::Get().GetState();
	keyboardTracker.Update(keyboardState);
}

bool Input::GetKey(Key input)
{
	return keyboardState.IsKeyDown(input);
}

bool Input::GetKeyDown(Key input)
{
	return keyboardTracker.IsKeyPressed(input);
}

bool Input::GetKeyUp(Key input)
{
	return keyboardTracker.IsKeyReleased(input);
}

Vector2 Input::GetMouse()
{
	auto state = DirectX::Mouse::Get().GetState();
	return Vector2((float)state.x, (float)state.y);
}

void Input::MouseMode(CursorMode mode)
{
	if (mode == CursorMode::Free) DirectX::Mouse::Get().SetMode(DirectX::Mouse::MODE_ABSOLUTE);
	else DirectX::Mouse::Get().SetMode(DirectX::Mouse::MODE_RELATIVE);
}

void Input::ResetKeys()
{
	keyboardTracker.Reset();
}