#pragma once

#include "Maths.h"

class Camera;

class Transform
{
	friend Camera;

private:

	Matrix worldMatrix;
	Matrix matrixPosition;
	Matrix matrixRotation;
	Matrix matrixScale;

	void Update();

public:
	Vector3 position;
	Vector3 eulerAngles;
	Vector3 scale;

	Transform();
	~Transform() {}

	// Translate object along vector X, Y, Z (World Coords)
	void Translate(Vector3 move);
	// Translate object along vector X, Y, Z (Local Coords)
	void TranslateLocal(Vector3 move);

	// Rotate object on axis X, Y, Z
	void Rotate(Vector3 spin);
	
	// Increase Scale of object by X, Y, Z
	void Scale(Vector3 scale);
	// Increase Scale of object by uniform scale
	void Scale(float scale);
	
	void GetMatrix(Matrix* matrix);
};

