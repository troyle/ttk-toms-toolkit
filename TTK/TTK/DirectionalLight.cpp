#include "DirectionalLight.h"

DirectionalLight::DirectionalLight()
{
	isActive = true;

	range = Maths::Infinity;

	colour = Colour::White;
	intensity = 1.0f;
	direction = Vector3(-1.0f, -1.0f, 0.0f);	
}
