#pragma once

#include "Maths.h"

class Colour
{
private:
	
public:
	float r, g, b, a;

	static Colour Red;
	static Colour Green;
	static Colour Blue;
	static Colour Black;
	static Colour White;

	Colour(); 
	Colour(float R, float G, float B, float A);
	Colour(float R, float G, float B);
	Colour(float Shade);
	~Colour() {}

	Vector3 AsVector3();
	Vector4 AsVector4();
};

