#include "Transform.h"

Transform::Transform()
{
	position = Vector3(0.0f, 0.0f, 0.0f);
	eulerAngles = Vector3(0.0f, 0.0f, 0.0f);
	scale = Vector3(1.0f, 1.0f, 1.0f);

	Update();
}

void Transform::Update()
{
	worldMatrix =	Matrix::CreateScale(scale) *
					Matrix::CreateRotationZ(eulerAngles.z * 0.0174532925f) *
					Matrix::CreateRotationX(eulerAngles.x * 0.0174532925f) *
					Matrix::CreateRotationY(eulerAngles.y * 0.0174532925f) *
					Matrix::CreateTranslation(position);
}
void Transform::GetMatrix(Matrix* matrix)
{
	Update();

	*matrix = worldMatrix;
}

void Transform::Translate(Vector3 move)
{
	position += move;
}
void Transform::TranslateLocal(Vector3 pos)
{
	position += Vector3(&matrixRotation._31) * pos.z + Vector3(&matrixRotation._11) * pos.x + Vector3(&matrixRotation._21) * pos.y;
}

void Transform::Rotate(Vector3 spin)
{
	eulerAngles += spin;
}

void Transform::Scale(Vector3 size)
{
	scale += size;
}
void Transform::Scale(float size)
{
	scale.x += size;
	scale.y += size;
	scale.z += size;
}