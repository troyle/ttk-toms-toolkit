#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <windowsx.h>
#include <string>

#ifdef _DEBUG
#pragma comment(lib, "DirectXTK_DEBUG.lib")
#else
#pragma comment(lib, "DirectXTK_RELEASE.lib")
#endif

#include <DirectXTK/DDSTextureLoader.h>

//////////////////////////////
//Custom Classes
#include "UIEngine.h"

#include "Maths.h"
#include "Containers.h"

#include "Camera.h"
#include "Input.h"

#include "DefaultShader.h"

#include "DirectionalLight.h"
#include "PointLight.h"

#include "Time.h"

#include "AssetManager.h"

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "dxgi.lib")
#pragma comment (lib, "user32.lib")

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
#define NEAR_CLIP 0.1f
#define FAR_CLIP 1000.0f

enum class ShaderType
{
	Vertex,
	Pixel,
};

class DXEngine
{
private:
	///////////////////////////////////////////////
	// Windows and Engine Data
	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;
	Matrix worldMatrix;
	Matrix orthoMatrix;
	Matrix viewMatrix;
	Matrix projMatrix;
	
	HWND savedHWND;
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	IDXGISwapChain* swapChain;
	ID3D11RenderTargetView* renderTargetView;
	ID3D11Texture2D* depthBuffer;
	ID3D11DepthStencilState* depthState;
	ID3D11DepthStencilState* depthDisabledState;
	ID3D11DepthStencilView* depthView;
	ID3D11RasterizerState* rasterState;
	ID3D11RasterizerState* rasterWireframeState;
	D3D11_VIEWPORT viewport;

	UIEngine* uiDisplay;

	int vRamSize;
	char gpuDesc[128];

	Colour clearColour = Colour::Black;
	
	void Release();

	void BeginScene();
	void EndScene();
	
	void ZBufferOff();
	void ZBufferOn();
	
	void SetBackBufferRenderTarget();
	void ResetViewport();
	
	/////////////////////////////////////////////////
	// Shader
	DefaultShader* defaultShader;
	
	// Can only render one camera at once... for now
	Camera* mainCamera;

	/////////////////////////////////////////////////
	// Storage for requested objects
	List<Model*> modelList;
	List<Light*> lightList;
		
	bool terminate = false;

public:
	
	DXEngine(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
	~DXEngine() {}

	bool Init();

	void SolidView();
	void WireframeView();

	Camera* CreateCamera(Vector3 position = Vector3(0.0f, 0.0f, 0.0f));
	void SetCamera(Camera* cam);

	Model* CreateModel(std::string filename);
	Model* CreateModel(Primitive pimitiveType, int resolution = 0);

	Texture* CreateTexture(std::string filename);

	DirectionalLight* CreateDirectionalLight();
	PointLight* CreatePointLight(Vector3 position = Vector3(0.0f, 0.0f, 0.0f));

	bool IsRunning();
	void Close();
	void Render();
};
