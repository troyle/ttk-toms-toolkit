#include "Camera.h"

void Camera::Update()
{
	DirectX::XMVECTOR upVec, posVec, lookAtVec;
		
	upVec = DirectX::XMLoadFloat3(&GlobalUp);
	posVec = DirectX::XMLoadFloat3(&transform.position);
	lookAtVec = DirectX::XMLoadFloat3(&GlobalForward);

	float pitch	= transform.eulerAngles.x * 0.0174532925f;
	float yaw	= transform.eulerAngles.y * 0.0174532925f;
	float roll	= transform.eulerAngles.z * 0.0174532925f;

	transform.matrixRotation = DirectX::XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	lookAtVec = XMVector3TransformCoord(lookAtVec, transform.matrixRotation);
	upVec = XMVector3TransformCoord(upVec, transform.matrixRotation);

	lookAtVec = DirectX::XMVectorAdd(posVec, lookAtVec);

	viewMatrix = DirectX::XMMatrixLookAtLH(posVec, lookAtVec, upVec);
}

void Camera::GetMatrix(Matrix* matrix)
{
	*matrix = viewMatrix;
}

void Camera::SetBaseViewMatrix()
{
	Update();
	baseViewMatrix = viewMatrix;
}

void Camera::GetBaseMatrix(Matrix* matrix)
{
	*matrix = baseViewMatrix;
}
