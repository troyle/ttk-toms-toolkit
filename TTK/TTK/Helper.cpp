#include "Helper.h"

wchar_t* Helper::string = nullptr;

const wchar_t* Helper::String2WCHAR(std::string in)
{
	size_t newsize = strlen(in.c_str()) + 1;
	if (string)
		delete string;
	string = new wchar_t[newsize];

	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars, string, newsize, in.c_str(), _TRUNCATE);

	return string;
}

std::string Helper::Int2String(int in)
{
	return "";
}

void Helper::Release()
{
	if (string)
		delete string;
}