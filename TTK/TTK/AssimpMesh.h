#pragma once

#include <vector>

#pragma comment(lib, "assimp-vc140-mt.lib")

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Maths.h"

//class Model;
class AssetManager;

class AssimpMesh
{
	//friend Model;
	friend AssetManager;

private:
	struct MeshData
	{
		Vector3 pos;
		Vector2 uv;
		Vector3 normal;
	};

	bool loaded;

	//Forget about textures for now
	std::vector<int> materialIndices;
	int numMaterials;

	std::vector<int> meshStartIndices;
	std::vector<int> meshSizes;

	std::string errorMsg;

public:
	int numFaces;
	int numVerts;
	MeshData* mesh;

	AssimpMesh(std::string);
	AssimpMesh() {}
	~AssimpMesh();

	bool isLoaded();
	std::string GetErrorMessage();

	ID3D11Buffer *vertexBuffer, *indexBuffer;
	D3D_PRIMITIVE_TOPOLOGY topologyType;

	bool InitBuffers(ID3D11Device* device, unsigned long* indices = nullptr, int indexSize = 0);
	void RenderBuffers(ID3D11DeviceContext* device);

	void Release();
};

