#include "AssetManager.h"

std::map <std::string, AssimpMesh*> AssetManager::meshMap;
std::map <std::string, Texture*> AssetManager::textureMap;

AssetManager::AssetManager()
{
	
}

AssetManager::~AssetManager()
{

}

AssimpMesh* AssetManager::CreateMesh(ID3D11Device* device, std::string filename, Primitive primitiveType, int resolution)
{
	if (meshMap.find(filename) == meshMap.end()) //Not in map
	{
		if (primitiveType == Primitive::Cube && resolution == -1) //Load in model file
		{
			AssimpMesh* assimpMesh = new AssimpMesh(filename);

			if (assimpMesh->isLoaded())
			{
				assimpMesh->InitBuffers(device);
				meshMap[filename] = assimpMesh;
				return assimpMesh;
			}
			else
			{
				//MessageBox(savedHWND, assimpMesh->GetErrorMessage().c_str(), "Error", 0);
				delete assimpMesh;
				return nullptr;
			}
		}
		else 
		{
			if (resolution < 0 || resolution > 1000000)
				return nullptr;

			AssimpMesh* newMesh = new AssimpMesh();
			newMesh->mesh = nullptr;
			unsigned long* indices = nullptr;
			int vertCount = 0;
			int index = 0;

			float size = 1.0f;

			switch (primitiveType)
			{
				case Primitive::Plane:
				{
					if (resolution == 0)
						resolution = 1;
					resolution += 1;

					float w, h;
					float u, v;

					newMesh->mesh = new AssimpMesh::MeshData[resolution*resolution * 6];
					indices = new unsigned long[resolution*resolution * 6];

					for (int y = 0; y < resolution; ++y)
					{
						w = Map((float)y, 0.0f, (float)resolution - 1.0f, 0.0f, size);
						u = Map((float)y, 0.0f, (float)resolution - 1.0f, 0.0f, 1.0f);
						for (int x = 0; x < resolution; ++x)
						{
							h = Map((float)x, 0.0f, (float)resolution - 1.0f, 0.0f, size);
							v = Map((float)x, 0.0f, (float)resolution - 1.0f, 0.0f, 1.0f);

							newMesh->mesh[vertCount].pos = Vector3(h, 0.0f, w);
							newMesh->mesh[vertCount].uv = Vector2(u, v);
							newMesh->mesh[vertCount].normal = Vector3(0.0f, 1.0f, 0.0f);

							vertCount++;
						}
					}

					for (int x = 0; x < resolution - 1; ++x)
					{
						for (int y = 0; y < resolution - 1; ++y)
						{
							int idx = x * resolution + y;

							indices[index] = (idx);
							indices[index + 1] = (idx + resolution);
							indices[index + 2] = (idx + 1);
							indices[index + 3] = (idx + 1);
							indices[index + 4] = (idx + resolution);
							indices[index + 5] = (idx + resolution + 1);

							index += 6;
						}
					}

					newMesh->numVerts = index;

					break;
				}
				case Primitive::Cube:
				{
					if (resolution == 0)
						resolution = 2;
					resolution += 1;

					vertCount = 24;
					newMesh->mesh = new AssimpMesh::MeshData[vertCount];
					indices = new unsigned long[36];

					index = 0;
					int indicesIndex = 0;

					///////////////////////////
					// Face
					//		1			3				
					//	 
					//	
					//
					//
					//		0			2			

					//Front Face
					{
						newMesh->mesh[index + 0].pos = Vector3(-0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(0.0f, 0.0f, -1.0f);
								  
						newMesh->mesh[index + 1].pos = Vector3(-0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;
								  
						newMesh->mesh[index + 2].pos = Vector3(0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;
								  
						newMesh->mesh[index + 3].pos = Vector3(0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;
					
						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;
						
						index += 4;
						indicesIndex += 6;
					}

					//Right Face
					{
						newMesh->mesh[index + 0].pos = Vector3(0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(1.0f, 0.0f, 0.0f);

						newMesh->mesh[index + 1].pos = Vector3(0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 2].pos = Vector3(0.5f, -0.5f, 0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 3].pos = Vector3(0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;

						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;

						index += 4;
						indicesIndex += 6;
					}

					//Back Face
					{
						newMesh->mesh[index + 0].pos = Vector3(0.5f, -0.5f, 0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(0.0f, 0.0f, 1.0f);

						newMesh->mesh[index + 1].pos = Vector3(0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 2].pos = Vector3(-0.5f, -0.5f, 0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 3].pos = Vector3(-0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;

						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;

						index += 4;
						indicesIndex += 6;
					}

					//Left Face
					{
						newMesh->mesh[index + 0].pos = Vector3(-0.5f, -0.5f, 0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(-1.0f, 0.0f, 0.0f);

						newMesh->mesh[index + 1].pos = Vector3(-0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 2].pos = Vector3(-0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 3].pos = Vector3(-0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;

						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;

						index += 4;
						indicesIndex += 6;
					}

					//Bottom Face
					{
						newMesh->mesh[index + 0].pos = Vector3(-0.5f, -0.5f, 0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(0.0f, -1.0f, 0.0f);

						newMesh->mesh[index + 1].pos = Vector3(-0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 2].pos = Vector3(0.5, -0.5f, 0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 3].pos = Vector3(0.5f, -0.5f, -0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;

						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;

						index += 4;
						indicesIndex += 6;
					}

					//Top Face
					{
						newMesh->mesh[index + 0].pos = Vector3(-0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 0].uv = Vector2(0.0f, 0.0f);
						newMesh->mesh[index + 0].normal = Vector3(0.0f, 1.0f, 0.0f);

						newMesh->mesh[index + 1].pos = Vector3(-0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 1].uv = Vector2(0.0f, 1.0f);
						newMesh->mesh[index + 1].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 2].pos = Vector3(0.5f, 0.5f, -0.5f);
						newMesh->mesh[index + 2].uv = Vector2(1.0f, 0.0f);
						newMesh->mesh[index + 2].normal = newMesh->mesh[index].normal;

						newMesh->mesh[index + 3].pos = Vector3(0.5f, 0.5f, 0.5f);
						newMesh->mesh[index + 3].uv = Vector2(1.0f, 1.0f);
						newMesh->mesh[index + 3].normal = newMesh->mesh[index].normal;

						indices[indicesIndex + 0] = index + 0;
						indices[indicesIndex + 1] = index + 1;
						indices[indicesIndex + 2] = index + 2;
						indices[indicesIndex + 3] = index + 2;
						indices[indicesIndex + 4] = index + 1;
						indices[indicesIndex + 5] = index + 3;

						index += 4;
						indicesIndex += 6;

						newMesh->numVerts = indicesIndex;
						index = indicesIndex;
					}					
				}
			}

			if (newMesh->mesh)
			{				
				if (!newMesh->InitBuffers(device, indices, index))
				{
					delete newMesh;
					return nullptr;
				}

				meshMap[filename] = newMesh;
				return newMesh;
			}
			else
				return nullptr;
		}
	}
	else
		return meshMap[filename];
}

Texture* AssetManager::CreateTexture(ID3D11Device* device, std::string filename)
{
	if (textureMap.find(filename) == textureMap.end()) //Not in map
	{
		Texture* newTex = new Texture();
		if (!newTex->Init(device, filename))
		{
			delete newTex;
			return nullptr;
		}
		textureMap[filename] = newTex;
		return newTex;
	}
	else
		return textureMap[filename];
}

void AssetManager::Release()
{
	for (auto index : meshMap)
	{
		index.second->Release();
		delete index.second;
	}

	for (auto index : textureMap)
	{
		index.second->Release();
		delete index.second;
	}
}
