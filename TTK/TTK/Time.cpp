#include "Time.h"

INT64 Time::frequency = 0, Time::startTime = 0;
float Time::ticksPerMs = 0.0f, Time::_deltaTime = 0.0f;
float Time::deltaTime = 0.0f;
float Time::timeScale = 0.0f;

bool Time::Init()
{
	Time::_deltaTime = 0.0f;
	Time::timeScale = 1.0f;

	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&Time::frequency);
	if (Time::frequency == 0) return false;

	// Find out how many times the frequency counter ticks every millisecond.
	Time::ticksPerMs = (float)(Time::frequency);

	QueryPerformanceCounter((LARGE_INTEGER*)&Time::startTime);

	return true;
}

void Time::Update()
{
	INT64 currentTime;
	float difference;

	QueryPerformanceCounter((LARGE_INTEGER*)& currentTime);

	difference = (float)(currentTime - Time::startTime);

	Time::_deltaTime = (difference / Time::ticksPerMs) * Time::timeScale;

	Time::startTime = currentTime;

	//Naughty c++
	float* tempDelta;
	tempDelta = (float*)(&Time::deltaTime);
	*tempDelta = Time::_deltaTime;
}
