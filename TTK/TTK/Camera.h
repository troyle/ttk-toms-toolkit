#pragma once

#include "Transform.h"

class DXEngine;

class Camera
{
	friend DXEngine;

private:
	Matrix viewMatrix;
	Matrix baseViewMatrix;

	void SetBaseViewMatrix();
	void GetBaseMatrix(Matrix* baseMatrix);
public:

	Camera() {}
	~Camera() {}

	Transform transform;
	
	void Update();
	void GetMatrix(Matrix* matrix);

};

