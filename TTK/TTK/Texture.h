#pragma once

#include <Windows.h>

#include "Helper.h"

class Texture
{
private:
	ID3D11ShaderResourceView* texture;

public:
	Texture() {}
	~Texture() {}

	bool Init(ID3D11Device* device, std::string filename);
	void Release();

	ID3D11ShaderResourceView* GetTexture();
};
