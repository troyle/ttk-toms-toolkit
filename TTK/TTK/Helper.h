#pragma once

#include <string>

class Helper
{
private:
	static wchar_t* string;

public:
	static const wchar_t* String2WCHAR(std::string in);
	static std::string Int2String(int in);

	static void Release();
};